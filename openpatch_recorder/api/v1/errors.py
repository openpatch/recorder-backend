from openpatch_core.errors import error_manager as em


def invalid_json(errors):
    return em.make_json_error(
        400, message="Invalid JSON in request body", details=errors, code=1
    )
