from openpatch_recorder.models.snapshot import Snapshot
from openpatch_core.schemas import ma


class SnapshotSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Snapshot
        load_instance = True


SNAPSHOT_SCHEMA = SnapshotSchema()
SNAPSHOTS_SCHEMA = SnapshotSchema(many=True)
