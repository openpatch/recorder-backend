from marshmallow import fields
from openpatch_recorder.models.recording import Recording
from openpatch_core.schemas import ma
from openpatch_core.database import db
from openpatch_recorder.api.v1.schemas.snapshot import SnapshotSchema
from openpatch_recorder.api.v1.schemas.frame import FrameSchema


class RecordingSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Recording
        sqla_session = db.session
        load_instance = True

    snapshots = fields.Nested(SnapshotSchema, many=True)
    frames = fields.Nested(FrameSchema, many=True)


RECORDING_SCHEMA = RecordingSchema()
RECORDINGS_SCHEMA = RecordingSchema(many=True)
