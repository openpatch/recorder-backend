from openpatch_recorder.models.frame import Frame
from openpatch_core.schemas import ma
from openpatch_core.database import db


class FrameSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Frame
        sqla_session = db.session
        load_instance = True
        include_relationships = True


FRAME_SCHEMA = FrameSchema()
FRAMES_SCHEMA = FrameSchema(many=True)
