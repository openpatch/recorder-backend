from uuid import uuid4
from datetime import datetime
from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_core.jwt import jwt_required, jwt_roles_required, get_jwt_claims
from openpatch_recorder.api.v1 import api, errors
from openpatch_recorder.models.recording import Recording
from openpatch_recorder.api.v1.schemas.frame import FRAME_SCHEMA
from openpatch_recorder.api.v1.schemas.snapshot import SNAPSHOT_SCHEMA
from openpatch_recorder.api.v1.schemas.recording import (
    RECORDING_SCHEMA,
    RECORDINGS_SCHEMA,
)

BASE_URL = "/recordings"


@api.route(BASE_URL, methods=["GET", "POST"])
def recordings():
    if request.method == "GET":
        return get_recordings()
    return start_recording()


@jwt_roles_required(["admin"])
def get_recordings():
    """
    @api {post} /v1/recordings Get recordings
    @apiVersion 1.0.0
    @apiName GetRecording
    @apiGroup Recordings
    """

    recordings = Recording.query.all()
    return jsonify({"recordings": RECORDINGS_SCHEMA.dump(recordings)}), 200


def start_recording():
    recording_json = request.get_json()
    if not recording_json or not recording_json.get("recording_id", None):
        return errors.invalid_json("Missing")

    recording = Recording.query.get(recording_json.get("recording_id"))
    if not recording:
        recording = Recording(
            id=recording_json.get("recording_id"), start=datetime.now()
        )
        db.session.add(recording)
        db.session.commit()

    return jsonify({"recording_id": recording.id})


def stop_recording(recording_uuid):
    recording = Recording.query.get(recording_uuid)
    if not recording:
        return errors.invalid_json("UUID invalid")

    if not recording.end:
        recording.end = datetime.utcnow()
        db.session.commit()

    return jsonify({}), 200


@api.route(BASE_URL + "/<recording_uuid>", methods=["GET", "PUT"])
def recording(recording_uuid):
    if request.method == "GET":
        return get_recording(recording_uuid)
    return stop_recording(recording_uuid)


@jwt_required
def get_recording(recording_uuid):
    jwt_claims = get_jwt_claims()

    if not jwt_claims:
        return errors.invalid_json()
    recording = Recording.query.get(recording_uuid)
    if not recording:
        return errors.invalid_json("UUID invalid")

    return jsonify({"recording": RECORDING_SCHEMA.dump(recording)})


@api.route(BASE_URL + "/<recording_uuid>/frames", methods=["POST"])
def frames(recording_uuid):
    return post_frames(recording_uuid)


def post_frames(recording_uuid):
    frame_json = request.get_json()

    if not frame_json:
        return errors.invalid_json("No JSON")

    frame_json["recording"] = recording_uuid

    try:
        result = FRAME_SCHEMA.load(frame_json, session=db.session)
    except ValidationError as e:
        db.session.rollback()
        return errors.invalid_json(str(e))

    db.session.commit()

    return jsonify({}), 200
