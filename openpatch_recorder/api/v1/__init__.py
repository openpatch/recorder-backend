# isort:skip_file
import os
from flask import Blueprint
from flask_cors import CORS

api = Blueprint("api_v1", __name__, template_folder="templates", static_folder="static")

CORS(api, origins=[os.getenv("OPENPATCH_ORIGINS", "*")])

from openpatch_recorder.api.v1 import recording
