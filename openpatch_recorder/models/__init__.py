from openpatch_core.database import db
from openpatch_recorder.models import frame, recording, snapshot
from sqlalchemy import orm

orm.configure_mappers()
