from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class Snapshot(Base):
    __tablename__ = gt("snapshot")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    timestamp = db.Column(db.Integer, nullable=False)
    state = db.Column(db.JSON, nullable=False)
    recording_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("recording"))), nullable=False
    )

    recording = db.relationship("Recording", back_populates="snapshots")

    def __repr__(self):
        return "<Snapshot %r>" % self.id
