from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID


class Recording(Base):
    __tablename__ = gt("recording")

    id = db.Column(GUID(), nullable=False, primary_key=True)
    start = db.Column(db.DateTime, nullable=False)
    end = db.Column(db.DateTime)

    frames = db.relationship(
        "Frame", back_populates="recording", order_by="Frame.timestamp"
    )
    snapshots = db.relationship(
        "Snapshot", back_populates="recording", order_by="Snapshot.timestamp"
    )

    def __repr__(self):
        return "<Recording %r>" % self.id
