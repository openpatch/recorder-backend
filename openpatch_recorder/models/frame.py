from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class Frame(Base):
    __tablename__ = gt("frame")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    format = db.Column(db.String(128))
    type = db.Column(db.String(256), nullable=False)
    payload = db.Column(db.JSON)
    item_id = db.Column(db.Integer)
    task_id = db.Column(db.Integer)
    timestamp = db.Column(db.Integer, nullable=False)
    recording_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("recording"))), nullable=False
    )

    recording = db.relationship("Recording", back_populates="frames")

    def __repr__(self):
        return "<Frame %r>" % self.id
