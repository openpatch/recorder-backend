## [1.2.2](https://gitlab.com/openpatch/recorder-backend/compare/v1.2.1...v1.2.2) (2020-06-08)


### Bug Fixes

* mocking of data ([80a1a56](https://gitlab.com/openpatch/recorder-backend/commit/80a1a569b2dbb5e045941ca36989ace6fe77b04d))
