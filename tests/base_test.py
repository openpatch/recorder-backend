import base64
import json
import os

import httpretty
from flask_testing import TestCase
from openpatch_recorder import create_app
from openpatch_core.database import db
from tests.mock import mock


@httpretty.activate
class BaseTest(TestCase):
    def create_app(self):
        return create_app("testing")

    def setUp(self):
        # mock authentification serivce, every jwt token is valid
        base_url = os.getenv("OPENPATCH_AUTHENTIFICATION_SERVICE")
        url = "%s/v1/verify" % base_url
        httpretty.register_uri(httpretty.GET, url, body="{}")

        # mock database
        db.drop_all()
        db.create_all()
        mock()

        # create fake autherization headers with jwt tokens
        self.fake_headers = {}

        self.fake_headers["admin"] = make_fake_headers(
            {"id": 1, "username": "admin", "role": "admin"}
        )

        self.fake_headers["author"] = make_fake_headers(
            {"id": 2, "username": "author", "role": "author"}
        )

        self.fake_headers["user"] = make_fake_headers(
            {"id": 3, "username": "user", "role": "user"}
        )

    def tearDown(self):
        db.session.remove()
        db.drop_all()


def make_fake_headers(jwt_claims):
    token_template = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.%s.F3OLwaYV5mKgus82Soo2tE_ninlwlkg-5XtRt2njgu8"
    json_payload = json.dumps(
        {"user_claims": jwt_claims}, separators=(",", ":")
    ).encode("utf-8")
    encoded_token = (
        base64.urlsafe_b64encode(json_payload).replace(b"=", b"").decode("utf-8")
    )
    headers = {"Authorization": token_template % encoded_token}
    return headers
