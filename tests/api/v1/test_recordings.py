from openpatch_recorder.models.recording import Recording
from tests.base_test import BaseTest
from tests import mock
import uuid


class Recordings(BaseTest):
    def test_post_recordings(self):
        response = self.client.post("/v1/recordings", json={})
        self.assertEqual(response.status_code, 400)

        response = self.client.post(
            "/v1/recordings", json={"recording_id": uuid.uuid4()}
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("recording_id", response.json)

    def test_get_recordings(self):
        response = self.client.get("/v1/recordings")
        self.assertEqual(response.status_code, 401)

        response = self.client.get("/v1/recordings", headers=self.fake_headers["admin"])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json.get("recordings")), len(mock.recordings))

    def test_get_recording(self):
        response = self.client.get(
            "/v1/recordings/3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac"
        )
        self.assertEqual(response.status_code, 401)

    def test_put_recording(self):
        recording = Recording.query.get("3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac")
        self.assertIsNone(recording.end)
        response = self.client.put(
            "/v1/recordings/3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac"
        )
        self.assertEqual(response.status_code, 200)
        recording = Recording.query.get("3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac")
        self.assertIsNotNone(recording.end)

    def test_post_frames(self):
        recording = Recording.query.get("3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac")
        old_count = len(recording.frames)
        response = self.client.post(
            "/v1/recordings/3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac/frames"
        )
        self.assertEqual(response.status_code, 400)
        recording = Recording.query.get("3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac")
        self.assertEqual(old_count, len(recording.frames))

        response = self.client.post(
            "/v1/recordings/3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac/frames",
            json={"type": "highlight", "timestamp": 200},
        )
        self.assertEqual(response.status_code, 200)
        recording = Recording.query.get("3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac")
        self.assertEqual(old_count, len(recording.frames) - 1)

