from marshmallow import ValidationError
from openpatch_recorder.api.v1.schemas.recording import RECORDINGS_SCHEMA
from openpatch_core.database import db

recordings = [
    {
        "created_on": "2019-12-18T13:51:17",
        "frames": [
            {
                "created_on": "2019-12-18T14:02:21",
                "payload": {"doing_struff": {"nesting_stuff": "Hi"}},
                "recording": "3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac",
                "timestamp": 100,
                "type": "SOME_ACTION",
                "updated_on": "2019-12-18T14:02:21",
            },
            {
                "created_on": "2019-12-18T14:03:12",
                "payload": {"doing_struff": {"nesting_stuff": "Hi"}},
                "recording": "3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac",
                "timestamp": 120,
                "type": "SOME_ACTION",
                "updated_on": "2019-12-18T14:03:12",
            },
            {
                "created_on": "2019-12-18T14:03:46",
                "payload": {"inter": 1},
                "recording": "3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac",
                "timestamp": 300,
                "type": "SOME_OTHER_ACTION",
                "updated_on": "2019-12-18T14:03:46",
            },
        ],
        "snapshots": [],
        "start": "2019-12-18T13:51:17",
        "updated_on": "2019-12-18T14:17:23",
        "id": "3ed77bc5-00ad-4f71-b07c-2cc740c3e5ac",
    },
    {
        "created_on": "2019-12-18T14:32:36",
        "end": "2019-12-18T14:32:57",
        "frames": [],
        "snapshots": [],
        "start": "2019-12-18T14:32:36",
        "updated_on": "2019-12-18T14:32:57",
        "id": "6ec3ec68-775b-4567-bd77-4a59e85820b6",
    },
]


def mock():
    result = RECORDINGS_SCHEMA.load(recordings, session=db.session)

    db.session.add_all(result)
    db.session.commit()
